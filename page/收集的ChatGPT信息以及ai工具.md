
## OpenAI 和 GPT

- OpenAI 官网： https://platform.openai.com/overview

### ChatGPT

- 虚拟短信服务： https://sms-activate.org/cn/info/ChatGPT

- 注册 ChatGPT 的教程： https://sms-activate.org/cn/info/ChatGPT

- 免费网页版 GPT，基于 gpt-3.5-turbo API： https://freegpt.one/

- 免费网页版 GPT，可用自己的 key： http://ai.okmiku.com/chat/

- 电脑客户端： https://github.com/Bin-Huang/chatbox/blob/main/README-CN.md

- 手机安卓端：谷歌市场搜问天

- 登高望远工具：https://www.speeder.one/auth/register?code=23O4

### GPT 开发

- 如何快速开发一个 OpenAI/GPT 应用： https://github.com/easychen/openai-gpt-dev-notes-for-cn-developer

- 一键免费部署你的私人 ChatGPT 网页应用： https://github.com/Yidadaa/ChatGPT-Next-Web

## AI 工具

- AI 类工具搜索，可以推荐相似的工具： https://www.futurepedia.io/

- 正则工具，根据结果反向推到表达式： https://regex.ai/

- AI 开发者的搜素引擎： https://www.phind.com/

- AI 对话式搜索： https://www.perplexity.ai/

## Ai Code Dev

- Idea ai 代码开发插件，可以集成到 idea： https://codeium.com/

- 使用 AI 写、编辑并和你的代码对话：Write, edit, and chat about your code with a powerful AI： https://www.cursor.so/ （已收费，但未和 IDE 集成）